#! /usr/bin/env python3

''' Uses the one-time pad cipher to encrypt a string. '''

import random

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] # Yes, I know spaces and numbers are not a part of the alphabet.
alpha_len = len(alphabet) - 1

def prefs_file():
    ''' Reads the prefs file to determine if the user would like to have their messages written to a file or the console. '''
    try:
        prefs = open("prefs.txt", "r")
        file_interaction_mode = eval(prefs.readline())
    except:
        prefs = open("prefs.txt", "w")
        file_interaction_mode = input("Would you like to read and write messages through text files (Y) or through the console (N)? ")
        if file_interaction_mode.lower() == "y":

            file_interaction_mode = True
            prefs.write(str(file_interaction_mode))

        else:

            file_interaction_mode = False
            prefs.write(str(file_interaction_mode))

        print("Your preference has been stored in prefs.txt. Delete the file to reset the preference.")
    prefs.close()
    return file_interaction_mode

def shift_gen(message):
    ''' Generates 1 shift for every character in the message and saves it to a file. '''

    shifts = []

    for character in message:
        shifts.append(random.randint(0, alpha_len-2))

    file = open("shifts.txt", "w")

    for shift in shifts:
        file.write("%s\n" % shift)

    file.close()
    return shifts

def shift_char(character, shift, reverse=False):
    ''' Shifts a character by the specified shift. '''
    orig_pos = alphabet.index(character)
    if reverse == False:
        new_pos = orig_pos + shift

        if new_pos > alpha_len:
            new_pos -= alpha_len

        return alphabet[new_pos]

    else:
        new_pos = orig_pos - shift

        if new_pos < 0:
            new_pos += alpha_len

        return alphabet[new_pos]

def encrypt_message(string_message, shifts, decrypt=False):
    ''' Either encrypts the message or decrypts it. '''
    message = list(string_message)

    if decrypt == False:
        for index, character in enumerate(message):
            message[index] = shift_char(character, shifts[index])

    elif decrypt == True:
        for index, character in enumerate(message):
            message[index] = shift_char(character, shifts[index], reverse=True)

    return message

def main_menu():
    file_interaction_mode = prefs_file()
    while True:
        option = input("(E)ncrypt or (d)ecrypt? ")

        if option.lower() == "e":
            message = ""
            if file_interaction_mode:
                try:
                    message_file = open("message.txt", "r")
                    raw_message = message_file.readline().lower()
                    message = raw_message.rstrip('\n')
                except:
                    print('Nothing found inside message.txt.')
                    quit()
            else:
                message = input("Please enter your message to be encrypted. ").lower()

            gen_shift = input("Would you like to use generate new shifts? ")

            shifts = []
            if gen_shift.lower() == "y":
                shifts = shift_gen(message)
                print("Shifts written to shifts.txt in working directory.")
            else:
                file = open("shifts.txt", "r")

                for shift_str in file.readlines():
                    shifts.append(int(shift_str))
                print("Shifts read from shifts.txt in working directory.")

                file.close()

            emessage = ''.join(encrypt_message(message, shifts)) # Since message is returned as list, it must be joined

            if file_interaction_mode:
                message_file = open("message.txt", "w")
                message_file.write(emessage)
                message_file.close()
                print("Message written to message.txt file in working directory")
            else:
                print(emessage)

        elif option.lower() == "d":
            emessage = ""
            if file_interaction_mode:
                try:
                    message_file = open("message.txt", "r")
                    raw_emessage = message_file.readline().lower()
                    emessage = raw_emessage.rstrip('\n')
                    message_file.close()
                except:
                    print('Nothing found inside message.txt.')
                    quit()
            else:
                emessage = input("Enter encrypted message. ")

            print("Reading shifts.txt file in the working directory...")

            file = open("shifts.txt", "r")

            shifts = []
            for shift_str in file.readlines():
                shifts.append(int(shift_str))

            message = ''.join(encrypt_message(emessage, shifts, decrypt=True)) # Since message is returned as list, it must be joined

            if file_interaction_mode:
                message_file = open("message.txt", "w")
                message_file.write(message.upper())
                message_file.close()
                print("Message written to message.txt file in working directory")
            else:
                print(message.upper())

main_menu()
